//
//  MainViewController.h
//  Logo Maker Store
//
//  Created by iBuildX on 21/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Slider.h"

@interface MainViewController : UIViewController<UICollectionViewDelegate , UICollectionViewDataSource , UITableViewDelegate , UITableViewDataSource>


@property (weak, nonatomic) IBOutlet Slider *mainSlider;


@property (weak, nonatomic) IBOutlet UICollectionView *myLogosCollectionView;


@property (weak, nonatomic) IBOutlet UICollectionView *basicLogoCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *iconTypeLogoCollectionView;


@property (weak, nonatomic) IBOutlet UICollectionView *letterLogoCollectionView;

@property (weak, nonatomic) IBOutlet UITableView *menuTableView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuTopConstraint;


- (IBAction)menuAction:(UIButton *)sender;


- (IBAction)menuCloseAction:(UIButton *)sender;
- (IBAction)newAction:(UIButton *)sender;


@end
