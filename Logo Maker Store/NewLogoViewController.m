//
//  NewLogoViewController.m
//  Logo Maker Store
//
//  Created by iBuildX on 22/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "NewLogoViewController.h"
#import "NewLogoCollectionViewCell.h"


@import ExpandingMenu;
@interface NewLogoViewController (){
    ExpandingMenuButton *anotherExpandedBtn;
}

@end

@implementation NewLogoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.textCollectionView.delegate = self;
    self.textCollectionView.dataSource = self;
    [self setupMenuButton];
}


-(void)setupMenuButton {
    [self.view layoutIfNeeded];
    
    
    CGRect menuFrame = CGRectMake(self.mainCanvasView.frame.size.width - 44, self.mainCanvasView.frame.size.height - 10, 34, 34);
    
    
    anotherExpandedBtn = [[ExpandingMenuButton alloc] initWithFrame:menuFrame centerImage:[UIImage imageNamed:@"menuBtnMain"] centerHighlightedImage:[UIImage imageNamed:@"menuBtnMain"]];
    [self.view addSubview:anotherExpandedBtn];
    
    ExpandingMenuItem *undobtn = [[ExpandingMenuItem alloc] initWithSize:menuFrame.size image:[UIImage imageNamed:@"menuBtnUndo"] highlightedImage:[UIImage imageNamed:@"menuBtnUndo"] backgroundImage:[UIImage imageNamed:@"menuBtnUndo"] backgroundHighlightedImage:[UIImage imageNamed:@"menuBtnUndo"] itemTapped:^{
        NSLog(@"Mobile is Clicked") ;

    }];
    
    ExpandingMenuItem *redobtn = [[ExpandingMenuItem alloc] initWithSize:menuFrame.size image:[UIImage imageNamed:@"menuBtnRedo"] highlightedImage:[UIImage imageNamed:@"menuBtnRedo"] backgroundImage:[UIImage imageNamed:@"menuBtnRedo"] backgroundHighlightedImage:[UIImage imageNamed:@"menuBtnRedo"] itemTapped:^{
        NSLog(@"Drone is Clicked") ;

    }];
    ExpandingMenuItem *angleobtn = [[ExpandingMenuItem alloc] initWithSize:menuFrame.size image:[UIImage imageNamed:@"menuBtnAngle"] highlightedImage:[UIImage imageNamed:@"menuBtnAngle"] backgroundImage:[UIImage imageNamed:@"menuBtnAngle"] backgroundHighlightedImage:[UIImage imageNamed:@"menuBtnAngle"] itemTapped:^{
        NSLog(@"Drone is Clicked") ;
        
    }];
    
    ExpandingMenuItem *copyobtn = [[ExpandingMenuItem alloc] initWithSize:menuFrame.size image:[UIImage imageNamed:@"menuBtnCopy"] highlightedImage:[UIImage imageNamed:@"menuBtnCopy"] backgroundImage:[UIImage imageNamed:@"menuBtnCopy"] backgroundHighlightedImage:[UIImage imageNamed:@"menuBtnCopy"] itemTapped:^{
        NSLog(@"Drone is Clicked") ;
        
    }];
    
    ExpandingMenuItem *layerbtn = [[ExpandingMenuItem alloc] initWithSize:menuFrame.size image:[UIImage imageNamed:@"menuBtnLayer"] highlightedImage:[UIImage imageNamed:@"menuBtnLayer"] backgroundImage:[UIImage imageNamed:@"menuBtnLayer"] backgroundHighlightedImage:[UIImage imageNamed:@"menuBtnLayer"] itemTapped:^{
        NSLog(@"Drone is Clicked") ;
        
    }];

    
    
    [anotherExpandedBtn addMenuItems:@[undobtn , redobtn , angleobtn , copyobtn , layerbtn]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NewLogoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewLogoCollectionViewCell" forIndexPath:indexPath];
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
