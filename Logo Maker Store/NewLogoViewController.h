//
//  NewLogoViewController.h
//  Logo Maker Store
//
//  Created by iBuildX on 22/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface NewLogoViewController : UIViewController<UICollectionViewDelegate , UICollectionViewDataSource>




@property (weak, nonatomic) IBOutlet UICollectionView *textCollectionView;



@property (weak, nonatomic) IBOutlet UIView *mainCanvasView;


- (IBAction)backAction:(UIButton *)sender;


@end
