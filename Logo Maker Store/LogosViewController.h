//
//  LogosViewController.h
//  Logo Maker Store
//
//  Created by iBuildX on 22/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogosViewController : UIViewController<UICollectionViewDelegate , UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *logoCategoryTxt;






@property (weak, nonatomic) IBOutlet UICollectionView *mainLogoCollectionView;

- (IBAction)backAction:(UIButton *)sender;



@end
