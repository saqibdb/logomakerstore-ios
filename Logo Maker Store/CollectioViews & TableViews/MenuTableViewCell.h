//
//  MenuTableViewCell.h
//  Logo Maker Store
//
//  Created by iBuildX on 22/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *menuTitleTxt;




@end
