//
//  MainLogoCollectionViewCell.h
//  Logo Maker Store
//
//  Created by iBuildX on 21/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainLogoCollectionViewCell : UICollectionViewCell




@property (weak, nonatomic) IBOutlet UIImageView *mainLogoImageView;





@end
