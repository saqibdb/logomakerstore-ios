//
//  NewLogoCollectionViewCell.h
//  Logo Maker Store
//
//  Created by iBuildX on 24/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewLogoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UILabel *mainTitle;






@end
